package com.olympic;

import java.io.*;
import java.util.*;

public class Main {
    private static final int yearListIndex = 9;
    private static final int medalistIndex = 14;
    private static final int playerListIndex = 1;
    private static final int ageListIndex = 3;
    private static final int eventListIndex = 13;
    private static final int athleteGenderListIndex = 2;
    private static final int teamListIndex = 7;

    public static void main(String[] args) {
        List<OlympicData> requiredOlympicData = getOlympicData();
        getGoldMedalByYear(requiredOlympicData);
        getGoldMedalIn1980(requiredOlympicData);
        getEventWiseMedal(requiredOlympicData);
        getGoldMedalInFootball(requiredOlympicData);
        getMaximumGoldFemaleAthlete(requiredOlympicData);
        getAthleteInMoreThanThreeOlympic(requiredOlympicData);
        getMedalByEachTeamEachYear(requiredOlympicData);
    }

    public static List<OlympicData> getOlympicData() {
        List<OlympicData> events = new ArrayList<>();
        File file = new File("./src/athlete_events.csv");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String eachLine;
            reader.readLine();
            while ((eachLine = reader.readLine()) != null) {
                String[] eventInfo = eachLine.split(",");
                OlympicData olympicData = new OlympicData();
                if ((eventInfo[yearListIndex].length()) == 4) {
                    olympicData.setYearList(eventInfo[yearListIndex]);
                    olympicData.setMedalList(eventInfo[medalistIndex]);
                    olympicData.setPlayerList(eventInfo[playerListIndex]);
                    olympicData.setAgeList(eventInfo[ageListIndex]);
                    olympicData.setEventList(eventInfo[eventListIndex]);
                    olympicData.setAthleteGenderList(eventInfo[athleteGenderListIndex]);
                    olympicData.setTeamList(eventInfo[teamListIndex]);
                    events.add(olympicData);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        } catch (IOException e) {
            System.out.println("Can't Read " + file);
        }
        return events;
    }

    public static Map<Integer, Map<String, Integer>> getGoldMedalByYear(List<OlympicData> olympicData) {

        Map<Integer, Map<String, Integer>> numberOfGoldMedalWonByEachPlayer = new HashMap<>();

        for (OlympicData eventsData : olympicData) {
            String playerName = eventsData.getPlayerList();
            int year = Integer.parseInt(eventsData.getYearList());
            String medal = eventsData.getMedalList().replaceAll("\"", "");
            boolean isGoldMedal = medal.equals("Gold");
            if (isGoldMedal) {
                if (numberOfGoldMedalWonByEachPlayer.containsKey(year)) {
                    Map<String, Integer> numberOfGoldMedalWon = numberOfGoldMedalWonByEachPlayer.get(year);

                    if (numberOfGoldMedalWon.containsKey(playerName)) {

                        int goldMedalCount = numberOfGoldMedalWon.get(playerName);
                        numberOfGoldMedalWon.put(playerName, goldMedalCount + 1);

                    } else {
                        numberOfGoldMedalWon.put(playerName, 1);
                    }

                } else {

                    Map<String, Integer> numberOfGoldMedalWon = new HashMap<>();
                    numberOfGoldMedalWon.put(playerName, 1);

                    numberOfGoldMedalWonByEachPlayer.put(year, numberOfGoldMedalWon);
                }
            }
        }
        System.out.println("\n1.display Year Wise NumberOf GoldMedals WonBy EachPlayer\n".toUpperCase());
        System.out.println("\nYear" + "\t" + "Number Of Gold Medal");
        for (Map.Entry<Integer, Map<String, Integer>> entry : numberOfGoldMedalWonByEachPlayer.entrySet()) {
            System.out.println(entry.getKey() + "\t" + entry.getValue());
        }
        return numberOfGoldMedalWonByEachPlayer;
    }

    public static Map<String, Integer> getGoldMedalIn1980(List<OlympicData> olympicData) {
        Map<String, Integer> playerDetail = new TreeMap<>();
        for (OlympicData eventData : olympicData) {
            String year = eventData.getYearList().replaceAll("\"", "");
            String ageCheck = eventData.getAgeList().replaceAll("\"", "");
            int age = 100;
            if (!ageCheck.equals("NA")) age = Integer.parseInt(eventData.getAgeList().replaceAll("\"", ""));
            String medal = eventData.getMedalList().replaceAll("\"", "");
            String playerName = eventData.getPlayerList().replaceAll("\"", "");
            if (year.equals("1980") && medal.equals("Gold") && age < 30) {
                if (!playerDetail.containsKey(playerName)) playerDetail.put(playerName, age);
            }
        }
        System.out.println("\n2.display Athletes Who Won GoldMedal In 1980 And AgeIsLess Than 30Years\n".toUpperCase());
        for (Map.Entry<String, Integer> entry : playerDetail.entrySet()) {
            System.out.println("Name:- " + entry.getKey() + ",\t" + "Age= " + entry.getValue());
        }
        return playerDetail;
    }

    public static Map<String, Integer> getEventWiseMedal(List<OlympicData> olympicData) {
        Map<String, Integer> eventDetails = new TreeMap<>();
        for (OlympicData eventData : olympicData) {
            String medal = eventData.getMedalList().replaceAll("\"", "");
            String year = eventData.getYearList().replaceAll("\"", "");
            String eventName = eventData.getEventList().replaceAll("\"", "");
            if (year.equals("1980") && (medal.equals("Gold") || medal.equals("Silver") || medal.equals("Bronze"))) {
                if (eventDetails.containsKey(eventName)) {
                    eventDetails.put(eventName, eventDetails.get(eventName) + 1);
                } else {
                    eventDetails.put(eventName, 1);
                }
            }
        }
        System.out.println("\n3.event Wise Number Of Gold Silver Bronze Medals In Year 1980".toUpperCase());
        for (Map.Entry<String, Integer> entry : eventDetails.entrySet()) {
            System.out.println(entry.getKey() + "\t" + entry.getValue());
        }
        return eventDetails;
    }

    public static Map<String, String> getGoldMedalInFootball(List<OlympicData> olympicData) {
        Map<String, String> footballGoldMedal = new HashMap<>();
        for (OlympicData eventData : olympicData) {
            String medal = eventData.getMedalList().replaceAll("\"", "");
            String eventName = eventData.getEventList().replaceAll("\"", "");
            String playerName = eventData.getPlayerList().replaceAll("\"", "");
            if (medal.equals("Gold") && (eventName.equals("Football Men's Football") || eventName.equals("Football Women's Football"))) {
                if (!footballGoldMedal.containsKey(playerName)) footballGoldMedal.put(playerName, eventName);
            }
        }
        System.out.println("\n4.display Gold Winner Of Football Of EveryOlympic\n".toUpperCase());
        for (Map.Entry<String, String> entry : footballGoldMedal.entrySet()) {
            System.out.println("Name:- " + entry.getKey() + ",\tEventName:- " + entry.getValue());
        }
        return footballGoldMedal;
    }

    public static Map<String, Integer> getMaximumGoldFemaleAthlete(List<OlympicData> olympicData) {
        Map<String, Integer> athleteName = new HashMap<>();
        for (OlympicData eventData : olympicData) {
            String athleteGender = eventData.getAthleteGenderList().replaceAll("\"", "");
            String medal = eventData.getMedalList().replaceAll("\"", "");
            String playerName = eventData.getPlayerList().replaceAll("\"", "");
            if (medal.equals("Gold") && athleteGender.equals("F")) {
                if (athleteName.containsKey(playerName)) {
                    athleteName.put(playerName, athleteName.get(playerName) + 1);
                } else {
                    athleteName.put(playerName, 1);
                }
            }
        }
        int max = 0;
        String name = "";
        for (Map.Entry<String, Integer> entry : athleteName.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                name = entry.getKey();
            }
        }
        System.out.println("\n5.female Athlete Won Maximum Number Of Gold All Olympics\n".toUpperCase());
        System.out.println("\nFemale Athlete '" + name.toUpperCase() + "' Has Won " + max + " Gold.\n");
        return athleteName;
    }

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    public static Map<String, String> getAthleteInMoreThanThreeOlympic(List<OlympicData> olympicData) {
        Map<String, String> athleteName = new TreeMap<>();
        Map<String, Integer> athleteNameInMoreOlympicGame = new TreeMap<>();
        Set<String> athleteSet = new HashSet<>();
        for (OlympicData eventData : olympicData) {
            String playerName = eventData.getPlayerList().replaceAll("\"", "");
            String year = eventData.getYearList().replaceAll("\"", "");
            athleteName.put(playerName, year);
            athleteSet.add(playerName.concat(year));
        }
        for (String set : athleteSet) {
            String name = set.substring(0, set.length() - 4);
            if (athleteNameInMoreOlympicGame.containsKey(name))
                athleteNameInMoreOlympicGame.put(name, athleteNameInMoreOlympicGame.get(name) + 1);
            else athleteNameInMoreOlympicGame.put(name, 1);
        }
        System.out.println("\n6.name Of Athlete Participated In More Than Three Olympics\n".toUpperCase());
        for (Map.Entry<String, Integer> entry : athleteNameInMoreOlympicGame.entrySet()) {
            if (entry.getValue() >= 3)
                System.out.println("Name:- " + entry.getKey() + ",\tNo. Of Olympic Played :- " + entry.getValue());
        }
        return athleteName;
    }

    public static  Map<Integer, Map<String, Integer>>  getMedalByEachTeamEachYear(List<OlympicData> olympicData) {

        Map<Integer, Map<String, Integer>> numberOfMedalWonByEachTeam = new HashMap<>();

        for (OlympicData eventsData : olympicData) {
            String teamName = eventsData.getTeamList();
            int year = Integer.parseInt(eventsData.getYearList());
            String medal = eventsData.getMedalList().replaceAll("\"", "");

            if (medal.equals("Gold") || medal.equals("Silver") || medal.equals("Bronze")) {
                if (numberOfMedalWonByEachTeam.containsKey(year)) {
                    Map<String, Integer> numberOfGoldMedalWon = numberOfMedalWonByEachTeam.get(year);

                    if (numberOfGoldMedalWon.containsKey(teamName)) {

                        int totalMedalCount = numberOfGoldMedalWon.get(teamName);
                        numberOfGoldMedalWon.put(teamName, totalMedalCount + 1);

                    } else {
                        numberOfGoldMedalWon.put(teamName, 1);
                    }
                } else {
                    Map<String, Integer> numberOfGoldMedalWon = new HashMap<>();
                    numberOfGoldMedalWon.put(teamName, 1);

                    numberOfMedalWonByEachTeam.put(year, numberOfGoldMedalWon);
                }
            }
        }
        System.out.println("\n7.display the total Medal By Each Team in Each Year.\n".toUpperCase());
        System.out.println("\nYear" + "\t" + "total Number Of Medal Won By Each Team");
        for (Map.Entry<Integer, Map<String, Integer>> entry : numberOfMedalWonByEachTeam.entrySet()) {
            System.out.println(entry.getKey() + "\t" + entry.getValue());
        }
        return numberOfMedalWonByEachTeam;
    }
}