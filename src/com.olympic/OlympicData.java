package com.olympic;

public class OlympicData {

    private String yearList;
    private String medalList;
    private String playerList;
    private String ageList;
    private String eventList;
    private String athleteGenderList;
    private String teamList;

    public String getAthleteGenderList() {
        return athleteGenderList;
    }

    public void setAthleteGenderList(String athleteGenderList) {
        this.athleteGenderList = athleteGenderList;
    }

    public String getEventList() {
        return eventList;
    }

    public void setEventList(String eventList) {
        this.eventList = eventList;
    }

    public String getAgeList() {
        return ageList;
    }

    public String getTeamList(){
        return teamList;
    }

    public void setAgeList(String ageList) {
        this.ageList = ageList;
    }

    public String getYearList() {
        return yearList;
    }
    public void setYearList(String yearList) {
        this.yearList = yearList;
    }

    public String getMedalList() {
        return medalList;
    }

    public void setMedalList(String medalList) {
        this.medalList = medalList;
    }

    public String getPlayerList() {
        return playerList;
    }

    public void setPlayerList(String playerList) {
        this.playerList = playerList;
    }
    public void setTeamList(String teamList){
        this.teamList = teamList;
    }
}
