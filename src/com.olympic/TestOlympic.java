package com.olympic;

import org.junit.*;
import java.io.IOException;
import java.util.List;
import static junit.framework.Assert.*;

public class TestOlympic {

    static int beforeTestCount = 1;
    static int afterTestCount = 1;
    List<OlympicData> athleteEvents = Main.getOlympicData();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("after class");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("before test : " + beforeTestCount++);
    }

    @Test
    public void testCountOfAthleteEventsData() {
        int expectedResult = 268278;
        int actualResult = athleteEvents.size();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCountOfAthletesWhoWonGoldMedal() {
        int expectedResult = 351;
        int actualResult = Main.getGoldMedalIn1980(athleteEvents).size();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCountOfNumberOfGoldMedalWonByEachPlayersYearWise() {
        int expectedResult = 35;
        int actualResult = Main.getGoldMedalByYear(athleteEvents).size();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCountOlympicYearOfGoldWinnerOfFootball() {
        int expectedResult = 2312;
        int actualResult = Main.getMaximumGoldFemaleAthlete(athleteEvents).size();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testAthletesWonGoldMedal() throws IOException {
        assertTrue(Main.getGoldMedalByYear(athleteEvents).size() > 20);
    }

    @Test
    public void testEventWiseMedalsCount() throws IOException {
        assertNotNull(Main.getMaximumGoldFemaleAthlete(athleteEvents));
    }

    @Test
    public void testGoldMedalsPerPlayerPerYear() throws IOException {
        assertFalse(Main.getGoldMedalByYear(athleteEvents).size() < 1);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after test : " + afterTestCount++);
    }
}
